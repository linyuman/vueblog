package com.markhub.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * jwt工具类
 */
@Slf4j
@Data
@Component
@ConfigurationProperties(prefix = "markhub.jwt")
public class JwtUtils {
    private String secret;          //密钥
    private long expire;            //超期时间
    private String header;          //header

    /**
     * 生成jwt token
     */
    public String generateToken(long userid){
        Date nowDate=new Date();
        //过期时间
        Date expireDate=new Date(nowDate.getTime()+expire*1000);
        return Jwts.builder()
                .setHeaderParam("typ","JWT")
                .setSubject(userid+"")
                .setIssuedAt(nowDate)
                .setExpiration(expireDate)
                .signWith(SignatureAlgorithm.HS512,secret)
                .compact();
    }

    public Claims getClaimByToken(String token){
        try {
            return Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
        }catch (Exception e){
            log.debug("validate is token error",e);
            return null;
        }
    }

    /**
     * token会话是否过期
     * @param expiration
     * @return true:过期
     */
    public boolean isTokenExpired(Date expiration){
        return expiration.before(new Date());
    }

}
