package com.markhub.utils;

import com.markhub.Shiro.AccountProfile;
import org.apache.shiro.SecurityUtils;

/**
 * 用于判断
 */
public class ShiroUtil {
    public static AccountProfile getProfile(){
        return (AccountProfile) SecurityUtils.getSubject().getPrincipal();
    }
}
