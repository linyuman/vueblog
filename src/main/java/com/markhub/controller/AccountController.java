package com.markhub.controller;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.map.MapUtil;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.markhub.common.dto.LoginDto;
import com.markhub.common.lang.Result;
import com.markhub.entity.User;
import com.markhub.service.UserService;
import com.markhub.utils.JwtUtils;
import org.apache.catalina.security.SecurityUtil;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.security.Security;

@RestController
public class AccountController {

    @Autowired
    private UserService userService;
    @Autowired
    private JwtUtils jwtUtils;

    /**
     * 用户登录接口开发
     * @return
     */
    @PostMapping("/login")
    public Result login(@Validated @RequestBody LoginDto loginDto, HttpServletResponse response){
        //进行获取
        User user=userService.getOne(new QueryWrapper<User>().eq("username",loginDto.getUsername()));
        Assert.notNull(user,"用户不存在");
        //判断账号密码是否错误 因为是md5加密所以这里md5判断
        //全加密要都不加密比较，不然即使是正确的密码也或进入这个判断的
        if(!SecureUtil.md5(user.getPassword()).equals(SecureUtil.md5(loginDto.getPassword()))){
            //密码不同则抛出异常
            return Result.fail("密码不正确！");
        }
        String jwt=jwtUtils.generateToken(user.getId());
        //放在header里面，jwt延期不需要调用另外的接口，需要遇到respond
        response.setHeader("Authorization",jwt);
        response.setHeader("Access-control-Expose-Headers","Authorization");
        return Result.sucessed(MapUtil.builder()
                .put("id",user.getId())
                .put("username",user.getUsername())
                .put("avatar",user.getAvatar())
                .put("email",user.getEmail())
                .map());
    }

    /**
     * 退出实现
     * 退出交给shiro直接退出
     * @return
     */
    @GetMapping("/logout")
    public Result logout(){
        //需要认证权限才能退出登录
        SecurityUtils.getSubject().logout();
        return Result.sucessed(null);
    }
}
