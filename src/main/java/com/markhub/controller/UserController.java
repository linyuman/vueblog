package com.markhub.controller;


import com.markhub.common.lang.Result;
import com.markhub.entity.User;
import com.markhub.service.UserService;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author linyuman
 * @since 2021-07-07
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequiresAuthentication
    @GetMapping("/index")
    public Result index(){
        Object user=userService.getById(1L);
        return Result.sucessed(user);
    }

    /**
     * 测试添加的校验
     * @Validated：校验的注解
     * @return
     */
    @PostMapping("/save")
    public Result save(@Validated @RequestBody User user){

        return Result.sucessed(user);
    }
}
