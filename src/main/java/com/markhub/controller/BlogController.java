package com.markhub.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.markhub.common.lang.Result;
import com.markhub.entity.Blog;
import com.markhub.service.BlogService;
import com.markhub.utils.ShiroUtil;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author linyuman
 * @since 2021-07-07
 */
@RestController
public class BlogController {


    @Autowired
    private BlogService blogService;

    /**
     * 没有的时候默认为1
     * @param currentPage
     * @return
     */
    @GetMapping("/blogs")
    public Result list(@RequestParam(defaultValue = "1")Integer currentPage){
        Page page=new Page(currentPage,5);
        IPage pageData=blogService.page(page,new QueryWrapper<Blog>().orderByDesc("created"));
        return Result.sucessed(pageData);
    }

    /**
     * @PathVariable：表示动态路由
     * @param id
     * @return
     */
    @GetMapping("/blog/{id}")
    public Result detail(@PathVariable(name = "id")Long id){
        Blog blog=blogService.getById(id);
        //判断是否为空 为空则断言异常
        Assert.notNull(blog,"该博客已经删除");
        return Result.sucessed(null);
    }


    /**
     * 编辑与添加同时的【如果查询到有id则表示编辑，没有id则表示添加】
     * @RequiresAuthentication：认证权限，登录之后才可以编辑
     * @param blog
     * @return
     */
    @RequiresAuthentication
    @PostMapping("/blog/edit")
    public Result edit(@Validated @RequestBody Blog blog){

        Blog temp=null;
        if(blog.getId()!=null){
            temp=blogService.getById(blog.getId());
            //只能编辑自己的文案【判断是否是自己的文案】
            Assert.isTrue(temp.getUserId().longValue()==ShiroUtil.getProfile().getId().longValue(),"没有权限编辑");

        }else {

            //添加操作
            temp=new Blog();
            temp.setUserId(ShiroUtil.getProfile().getId());
            temp.setCreated(LocalDateTime.now());
            temp.setStatus(0);
        }
        //将blog的值赋给temp 忽略 id userid created status 引用于hutool
        BeanUtil.copyProperties(blog,temp,"id","userId","created","status");
        blogService.saveOrUpdate(temp);
        return Result.sucessed(null);
    }
}
