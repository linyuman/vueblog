package com.markhub.mapper;

import com.markhub.entity.Blog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author linyuman
 * @since 2021-07-07
 */
public interface BlogMapper extends BaseMapper<Blog> {

}
