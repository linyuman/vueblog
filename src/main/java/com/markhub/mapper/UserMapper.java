package com.markhub.mapper;

import com.markhub.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author linyuman
 * @since 2021-07-07
 */
public interface UserMapper extends BaseMapper<User> {

}
