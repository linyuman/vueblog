package com.markhub.Shiro;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Data
public class AccountProfile implements Serializable {
    private Long id;

    private String username;

    /**
     * 头像
     */
    private String avatar;

    private String email;

}
