package com.markhub.Shiro;

import cn.hutool.core.bean.BeanUtil;
import com.markhub.entity.User;
import com.markhub.service.UserService;
import com.markhub.utils.JwtUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AccountRealm extends AuthorizingRealm {

    @Autowired
    private JwtUtils jwtUtils;
    @Autowired
    private UserService userService;
    /**
     * 为了让realm支持jwt的凭证校验
     * @param token
     * @return
     */
    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JwtToken;
    }

    /**
     * 权限校验
     * 拿到用户之后，获取用户的权限信息，封装之后返回给shiro
     * @param principalCollection
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {

        return null;
    }

    /**
     * 登录校验
     * 返回校验之后的基本信息
     * @param authenticationToken
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        //获取到jwt
        JwtToken jwtToken=(JwtToken)authenticationToken;
        //获取userId
        String userId=jwtUtils.getClaimByToken((String)jwtToken.getPrincipal()).getSubject();
        //获取用户内容
        User user=userService.getById(Long.valueOf(userId));
        if(user==null){
            throw new UnknownAccountException("账户不存在！");
        }
        //用户被锁定
        if (user.getStatus()==1){
            throw new LockedAccountException("账户已被锁定！");
        }
        AccountProfile profile=new AccountProfile();
        //值转移【将user数据转移到profile】
        BeanUtil.copyProperties(user,profile);
        //获取用户信息  密钥token 用户名字
        return new SimpleAuthenticationInfo(profile,jwtToken.getCredentials(),getName());
    }
}
