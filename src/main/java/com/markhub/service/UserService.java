package com.markhub.service;

import com.markhub.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author linyuman
 * @since 2021-07-07
 */
public interface UserService extends IService<User> {

}
