package com.markhub.service;

import com.markhub.entity.Blog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author linyuman
 * @since 2021-07-07
 */
public interface BlogService extends IService<Blog> {

}
